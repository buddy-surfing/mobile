export class Buddy {
  id_buddy: number;
  name: string;
  bio: string;
  hosting: boolean;
  guided_tour: boolean;
  invite_drink: boolean;
  city_name: string;
  languages: object;
  bio_image_url: string;
}
