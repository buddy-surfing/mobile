import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environment/environment';

@Pipe({
  name: 'image',
})
export class ImagePipe implements PipeTransform {
  transform(value: string, ...args) {
    return environment.apiUrl + value;
  }
}
