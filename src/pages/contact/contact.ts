import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Buddy } from '../../model/buddy';
import { EmailComposer } from '@ionic-native/email-composer';

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  buddyData: Buddy;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private emailComposer: EmailComposer) {
  }

  ionViewDidLoad() {
    this.buddyData = this.navParams.get('buddy');
  }

  mailto(email) {
    this.emailComposer.addAlias('gmail', 'com.google.android.gm');
    this.emailComposer.open({
      app: 'gmail',
      to: email
    });
  }

  openSkype(skype) {
    window.open('skype:'+skype);
  }

}
