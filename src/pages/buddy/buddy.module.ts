import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuddyPage } from './buddy';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    BuddyPage,
  ],
  imports: [
    IonicPageModule.forChild(BuddyPage),
    PipesModule
  ],
})
export class BuddyPageModule {}
