import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BuddyProvider } from '../../providers/buddy/buddy';
import { Buddy } from '../../model/buddy';
import { ContactPage } from '../contact/contact';

@IonicPage()
@Component({
  selector: 'page-buddy',
  templateUrl: 'buddy.html',
})
export class BuddyPage {

  city_name: string;

  buddies: Buddy[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private buddyService: BuddyProvider) {
  }

  ionViewDidLoad() {
    this.city_name = this.navParams.get('city_name');
    this.buddyService.findAllBuddies(this.city_name)
      .subscribe(budd => {
        this.buddies = budd; 
      });
  }

  public goToContact(buddy: Buddy) {
    console.log(buddy);
    this.navCtrl.push(ContactPage, { buddy: buddy });
  }

}
