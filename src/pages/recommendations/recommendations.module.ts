import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecommendationsPage } from './recommendations';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    RecommendationsPage,
  ],
  imports: [
    IonicPageModule.forChild(RecommendationsPage),
    PipesModule
  ],
})
export class RecommendationsPageModule {}
