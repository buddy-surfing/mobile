import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RecommendationsProvider } from '../../providers/recommendations/recommendations';
import { Recommendation } from '../../model/recommendations';

@IonicPage()
@Component({
  selector: 'page-recommendations',
  templateUrl: 'recommendations.html',
})
export class RecommendationsPage {
  city_name: string;

  recommendations: Recommendation[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private recommendationsProv: RecommendationsProvider) {
  }

  ionViewDidLoad() {
    this.city_name = this.navParams.get('city_name');
    this.recommendationsProv.findAllRecommendations(this.city_name)
      .subscribe(recom => {
        this.recommendations = recom;
      });
  }

}
