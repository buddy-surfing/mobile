import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CityProvider } from '../../providers/city/city';
import { City } from '../../model/city';
import { RecommendationsPage } from '../recommendations/recommendations';
import { BuddyPage } from '../buddy/buddy';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  cities: City[];
  constructor(public navCtrl: NavController,
    private cityProv: CityProvider) {

  }

  ionViewDidLoad() {
    this.cityProv.findAllCities().subscribe(cities => {
      this.cities = cities;
    });
  }

  public goToRecommendations(city_name: string) {
    this.navCtrl.push(RecommendationsPage, {
      city_name: city_name
    });
  }

  public goToBuddyPage(city_name: string) {
    this.navCtrl.push(BuddyPage, {
      city_name: city_name
    });
  }

}
