import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CityProvider } from '../providers/city/city';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from '../pipes/pipes.module';
import { RecommendationsPageModule } from '../pages/recommendations/recommendations.module';
import { BuddyPageModule } from '../pages/buddy/buddy.module';
import { BuddyProvider } from '../providers/buddy/buddy';
import { RecommendationsProvider } from '../providers/recommendations/recommendations';
import { ContactPageModule } from '../pages/contact/contact.module';

import { EmailComposer } from "@ionic-native/email-composer";

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    PipesModule,
    RecommendationsPageModule,
    BuddyPageModule,
    ContactPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CityProvider,
    BuddyProvider,
    RecommendationsProvider,
    EmailComposer
  ]
})
export class AppModule {}
