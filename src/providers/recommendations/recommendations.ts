import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environment/environment';
import { Recommendation } from '../../model/recommendations';

@Injectable()
export class RecommendationsProvider {

  constructor(public http: HttpClient) {
    console.log('Hello RecommendationsProvider Provider');
  }

  public findAllRecommendations(cityName: string) {
    const url = environment.apiUrl + 'api/Recommendations/' + cityName;
    return this.http.get<Recommendation[]>(url);
  }
}
