import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environment/environment';
import { City } from '../../model/city';

@Injectable()
export class CityProvider {

  constructor(public http: HttpClient) {
  }

  public findAllCities() {
    const url = environment.apiUrl + 'api/Cities';
    return this.http.get<City[]>(url);
  }

}
