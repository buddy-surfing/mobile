import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environment/environment';
import { Buddy } from '../../model/buddy';

@Injectable()
export class BuddyProvider {

  constructor(public http: HttpClient) {
  }
  
  public findAllBuddies(city: string) {
    const url = environment.apiUrl + 'api/Buddies/' + city;
    return this.http.get<Buddy[]>(url);
  }
}
